"
Bloc is a class implementing an element of a ListeChainee. It holds a single element, and has links to the Bloc before and after itself, if those Bloc exist.

Its API is
- element, prev, next
- element:, prev:, next:  

Its creation API is message new: 

Instance Variables :
	- element : anObject
	- prev : aBloc
	- next : aBloc
"
Class {
	#name : #Bloc,
	#superclass : #Object,
	#instVars : [
		'prev',
		'element',
		'next'
	],
	#category : #Chaine
}

{ #category : #'instance creation' }
Bloc class >> new: anObject [
	^ self new element: anObject.
 
]

{ #category : #accessing }
Bloc >> element [ 
	^ element
	
]

{ #category : #setting }
Bloc >> element: anObject [
	element := anObject
]

{ #category : #accessing }
Bloc >> next [
	^ next
]

{ #category : #setting }
Bloc >> next: aBloc [ 
	next := aBloc
]

{ #category : #accessing }
Bloc >> prev [
	^ prev
]

{ #category : #setting }
Bloc >> prev: aBloc [
	prev := aBloc
]

{ #category : #setting }
Bloc >> previous: aBlocPrev next: aBlocNext [
"links three blocks between them; aBlocPrev previous to self and aBlocNext next to self"
	aBlocPrev next: self.
	self prev: aBlocPrev; next: aBlocNext.
	aBlocNext prev: self
]

{ #category : #printing }
Bloc >> printOn: aStream [
	self element printOn: aStream
]
