"
ListeChainee is a class implementing a Linked List (different from the native one in Pharo). It has access to the head of the list (the first element) as well as the next element, and therefore can navigate through every other element. The size of the list is also kept in memory.
Those elements and the links between them are represented using the Bloc class from the same package.

Ex : 
| l |
l := ListeChainee startingWith: 4. l append: 3; append: 6.
l
>> #(4 3 6)

Its API is :
	- head, last, size, pop, +
	- head:, contains:, at:, append:
	- at:set:, at:insert:
	
Its creation API is startingWith:

Instance Variables :
	- head : aBloc
	- size : anInt
	- last : aBloc
"
Class {
	#name : #ListeChainee,
	#superclass : #Object,
	#instVars : [
		'head',
		'size',
		'last'
	],
	#category : #Chaine
}

{ #category : #initialization }
ListeChainee class >> startingWith: anObject [
	^ self new
	head: anObject;
	yourself
	
	
	
]

{ #category : #arithmetic }
ListeChainee >> + aListeChainee [
"concatenates two ListeChainee"
	| res |
	res := self class new.
	1 to: (self size) do: [ :i | res append: (self at: i) ].
	1 to: (aListeChainee size) do: [ :i | res append: (aListeChainee at: i) ].
	^ res
]

{ #category : #adding }
ListeChainee >> append: anObject [
"add anObject at the end of the list"
	| bloc empty |
	(size = 0) ifTrue: [ self head: anObject ]
				  ifFalse: [ empty := last next.
								 bloc := (Bloc new: anObject) previous: last next: empty.
								 size := size + 1.
								 last := bloc ]
	
]

{ #category : #accessing }
ListeChainee >> at: anInt [ 
"returns the element of index anInt, if anInt > size or anInt < 0, raise SubscriptOutOfBounds."
	| bloc | 
	(size < anInt or: anInt < 0) ifTrue: [ SubscriptOutOfBounds signalFor: anInt lowerBound: 1 upperBound: size ].
	bloc := head.
	1 to: (anInt - 1) do: [ :i | bloc := bloc next ].
	^ (bloc element) 
]

{ #category : #adding }
ListeChainee >> at: anInt insert: anObject [ 
"insert anObject at anInt index, the previous object at index anInt gets the anInt+1 index
 If anInt > size or anInt < 0, raise SubscriptOutOfBounds."
	| bloc insertedBloc |
	(size < anInt or: anInt < 0) ifTrue: [ SubscriptOutOfBounds signalFor: anInt lowerBound: 1 upperBound: size ].
	insertedBloc := Bloc new: anObject.
	bloc := head.
	1 to: anInt - 1 do: [ :i | bloc := bloc next ].
	insertedBloc previous: bloc prev next: bloc.
	size := size + 1
]

{ #category : #setting }
ListeChainee >> at: anInt set: anObject [
"Put anObject at index anInt, replacing the previous element. If anInt > size or anInt < 0, raise SubscriptOutOfBounds."
	| bloc |
	(size < anInt or: anInt < 0) ifTrue: [ SubscriptOutOfBounds signalFor: anInt lowerBound: 1 upperBound: size ].
	bloc := head.
	1 to: anInt - 1 do: [ :i | bloc := bloc next ].
	bloc element: anObject
]

{ #category : #comparing }
ListeChainee >> contains: anObject [
"looks if anObject is in the list, returns true if it is, false otherwise"
	| bloc |
	bloc := head.
	1 to: size do: [ :i | (bloc element = anObject) ifTrue: [ ^ true ] ifFalse: [ bloc := bloc next ] ].
	^ false	
]

{ #category : #accessing }
ListeChainee >> head [ 
	^ (head element)
]

{ #category : #setting }
ListeChainee >> head: anObject [
"anObject becomes the first element of the list; 
		if the list was empty, the size is set to one
		if the list was not empty, the link of the previous head to the tail of the list is transfered to the new one"
	| bloc |
	bloc := Bloc new: anObject.
	(size = 0) ifTrue: [ bloc previous: head next: head.
							  size := 1.
							  last := bloc. ]
				  ifFalse: [ bloc previous: head prev next: head ].
	head := bloc
]

{ #category : #initialization }
ListeChainee >> initialize [ 
	| bloc |
	bloc := EmptyBloc new.
	size := 0.
	head := bloc.
	last := bloc
]

{ #category : #accessing }
ListeChainee >> last [
	^ (last element)
]

{ #category : #removing }
ListeChainee >> pop [
"removes from the list the last element and returns it
 If list is empty, returns an EmptyBloc

Ex: (ListeChainee startingWith: 6) pop >> 6"
	| temp |
	temp := last.
	last := temp prev.
	last next: temp next.
	(size = 1) ifTrue: [ head := temp prev ].
	(size = 0) ifFalse: [ size := size - 1 ].
	^ (temp element)
		
]

{ #category : #printing }
ListeChainee >> printOn: aStream [
	| current |
	current := head.
	aStream nextPutAll: '#('.
	1 to: size do: [ :i | current printOn: aStream. (i = size) ifFalse: [ aStream nextPutAll: ' ' ] . current := current next  ].
	aStream nextPutAll: ')'
]

{ #category : #accessing }
ListeChainee >> size [
	^ size
]
