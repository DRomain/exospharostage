"
EmptyBloc is a class inheriting from Bloc, used to initialize a ListeChainee and limiting the number of null checks.

Its API is
- element, prev, next 

Its creation API is message new: 

Instance Variables :
	- element : self (anEmptyBloc)
	- prev : self (anEmptyBloc)
	- next : self (anEmptyBloc)
"
Class {
	#name : #EmptyBloc,
	#superclass : #Bloc,
	#category : #Chaine
}

{ #category : #comparing }
EmptyBloc >> = anEmptyBloc [ 
	^ (self class = anEmptyBloc class) 
]

{ #category : #accessing }
EmptyBloc >> element [ 
	^ self
]

{ #category : #accessing }
EmptyBloc >> next [
	^ self
]

{ #category : #accessing }
EmptyBloc >> prev [
	^ self
]
