"
This class contains tests for the class Bloc.
"
Class {
	#name : #BlocTest,
	#superclass : #TestCase,
	#category : #ChaineTest
}

{ #category : #tests }
BlocTest >> testCopyReturnsANewBloc [
	| bloc copiedBloc |
	bloc := Bloc new: 9.
	copiedBloc := bloc copy.
	bloc element: 4.
	self assert: bloc element equals: 4.
	self assert: copiedBloc element equals: 9 
]

{ #category : #tests }
BlocTest >> testInitialization [
	| instance |
	instance := Bloc new: 2.
	self assert: instance element equals: 2
]

{ #category : #tests }
BlocTest >> testPreviousNextCreatesLinksBetweenThreeBloc [
	| bloc1 bloc2 bloc3 |
	bloc1 := Bloc new: 1.
	bloc2 := Bloc new: 2.
	bloc3 := Bloc new: 3.
	bloc2 previous: bloc1 next: bloc3.
	self assert: bloc1 next element equals: 2.
	self assert: bloc2 prev element equals: 1.
	self assert: bloc2 next element equals: 3.
	self assert: bloc3 prev element equals: 2
]

{ #category : #tests }
BlocTest >> testPrintingElement [
	| bloc |
	bloc := Bloc new: 4.
	self assert: bloc printString equals: '4'
]
