"
This class contains tests for the class ListeChainee.
"
Class {
	#name : #ListeChaineeTest,
	#superclass : #TestCase,
	#category : #ChaineTest
}

{ #category : #tests }
ListeChaineeTest >> testAppendActualizesLastAndSize [
	| list |
	list := ListeChainee startingWith: 6.
	list append: 12.
	self assert: list last equals: 12.
	self assert: list size equals: 2.
]

{ #category : #tests }
ListeChaineeTest >> testAtInsertPutsElementInCorrectPlaceAndIncrementsSize [
	| list |
	list := ListeChainee startingWith: 1.
	list append: 3; append: 4; append: 5.
	list at: 2 insert: 2.
	1 to: 5 do: [ :i | self assert: (list at: i) equals: i ].
	self assert: list size equals: 5
]

{ #category : #tests }
ListeChaineeTest >> testAtInsertRaisesExceptionWhenIndexNegativeOrAboveSize [
	| list |
	list := ListeChainee startingWith: 6.
	list append: 12.
	list append: 4.
	self should: [ list at: 4 insert: 8 ] raise: SubscriptOutOfBounds.
	self should: [ list at: -2 insert: 8 ] raise: SubscriptOutOfBounds
]

{ #category : #tests }
ListeChaineeTest >> testAtReturnsTheElementCorrespondingToIndex [
	| list |
	list := ListeChainee startingWith: 6.
	list append: 12.
	list append: 4.
	list append: 3.
	self assert: (list at: 2) equals: 12.
	self assert: (list at: 4) equals: 3
]

{ #category : #tests }
ListeChaineeTest >> testAtSetRaisesExceptionWhenIndexNegativeOrAboveSize [
	| list |
	list := ListeChainee startingWith: 6.
	list append: 12.
	list append: 4.
	self should: [ list at: 4 set: 8 ] raise: SubscriptOutOfBounds.
	self should: [ list at: -2 set: 8 ] raise: SubscriptOutOfBounds
]

{ #category : #tests }
ListeChaineeTest >> testAtWithIndexAboveSizeRaisesException [
	| list |
	list := ListeChainee new.
	self should: [ list at: 3 ] raise: SubscriptOutOfBounds 
]

{ #category : #tests }
ListeChaineeTest >> testCountainsFunctional [
	| list |
	list := ListeChainee startingWith: 6.
	list append: 12.
	list append: 4.
	list append: 3.
	self assert: (list contains: 2) equals: false.
	self assert: (list contains: 4) equals: true
]

{ #category : #tests }
ListeChaineeTest >> testHeadAndLastReturnsAnEmptyBlocIfEmptyList [
	| list |
	list := ListeChainee new. 
	self assert: list head equals: EmptyBloc new.
	self assert: list last equals: EmptyBloc new
]

{ #category : #tests }
ListeChaineeTest >> testInitializationOfSizeAndElement [
	| list |
	list := ListeChainee startingWith: 6.
	self assert: list size equals: 1.
	self assert: list head equals: 6.
	self assert: list last equals: 6
]

{ #category : #tests }
ListeChaineeTest >> testListIsEmptyIfPopOnOneElementList [
	| list |
	list := ListeChainee startingWith: 6.
	list pop.
	self assert: list head equals: EmptyBloc new.
	self assert: list size equals: 0
]

{ #category : #tests }
ListeChaineeTest >> testPlusConcatenatesTwoListeChainees [
	| list1 list2 list3 |
	list1 := ListeChainee startingWith: 1.
	list1 append: 2; append: 3.
	list2 := ListeChainee startingWith: 4.
	list2 append: 5; append: 6.
	list3 := list1 + list2.
	1 to: 6 do: [ :i | self assert: (list3 at: i) equals: i ].
]

{ #category : #tests }
ListeChaineeTest >> testPopReturnsValueAndActualizesLastAndSize [
	| list |
	list := ListeChainee startingWith: 6.
	list append: 12.
	self assert: list pop equals: 12.
	self assert: list size equals: 1.
	self assert: list last equals: 6
]

{ #category : #tests }
ListeChaineeTest >> testPrintingListeChainee [
	| list |
	list := ListeChainee startingWith: 4.
	list append: 3; append: 7.
	self assert: list printString equals: '#(4 3 7)'
]

{ #category : #tests }
ListeChaineeTest >> testSetAtReplacesTheElementOfChoosenIndex [
	| list |
	list := ListeChainee startingWith: 6.
	list append: 12.
	list append: 4.
	list at: 2 set: 8.
	self assert: (list at: 1) equals: 6.
	self assert: (list at: 2) equals: 8.
	self assert: (list at: 3) equals: 4
]

{ #category : #tests }
ListeChaineeTest >> testSizeEqualsZeroIfEmptyList [
	| list |
	list := ListeChainee new. 
	self assert: list size equals: 0
]
