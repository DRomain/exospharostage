"
A handler of sorts for ClassDocBuilder, creates a simple documentation for every class in a given package.

Ex:

PackageDocBuilder writeDoc: ListeChainee package (produces the documentation for every class in package Chaine)
"
Class {
	#name : #PackageDocBuilder,
	#superclass : #ClassDocBuilder,
	#category : #DocBuilder
}

{ #category : #writing }
PackageDocBuilder class >> writeDoc: aRPackage [
"writes the documentation of every class in aRPackage"
 aRPackage definedClasses do: [ :each | ClassDocBuilder writeDoc: each ]
]
