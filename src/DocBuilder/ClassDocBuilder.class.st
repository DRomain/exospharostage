"
ClassDocBuilder receives a class and creates an .md file describing the class, stating its name, superclass, comment, package name, subclasses if they exist, instance and class variables and accessors.
The files are placed in the ""documents"" folder, in a folder ""PharoDocBuild"", then finally in a folder named after the package of the class. 

Ex: ClassDocBuilder writeDoc: ListeChainee (produces documentation for ListeChainee)

Its API (only on class side) is : 
- writeName, writeComment, writeSuperclass, writePackage, writeComment, writeSubclasses, writeSelectors, writeClassVariables, writeInstanceVariables
- doesFileAlreadyExists
- createFileStream 
- writeDoc:

Class Variables :

	- class : aClass (documentation is produced for this class)
	- fileStream : aStream (writing stream to produce the documentation file)
"
Class {
	#name : #ClassDocBuilder,
	#superclass : #Object,
	#classInstVars : [
		'class',
		'fileStream'
	],
	#category : #DocBuilder
}

{ #category : #enumerating }
ClassDocBuilder class >> createFileStream [
	"create the file needed in the directory Documents / PharoDocBuild / PackageName and returns a writing stream to this file"

	| doc |
	doc := FileLocator documents.
	fileStream := (doc / 'PharoDocBuild' / class package name / class name , 'md') ensureCreateFile writeStream 
]

{ #category : #delegated }
ClassDocBuilder class >> doesFileAlreadyExists [
"checks whether or not the documentation is already written (or if a file having the same name exists)"
	^ (FileLocator documents / 'PharoDocBuild' / class package name / class name , '.md') asFileReference exists
]

{ #category : #writing }
ClassDocBuilder class >> writeClassVariables [
"writes in the file the name of the class variables"
	fileStream nextPutAll: String cr; nextPutAll: '## Class Variables'; nextPutAll: String cr; nextPutAll: String cr.
	class classVariables do: [ :each | fileStream nextPutAll: '- ';
																nextPutAll: each;
																nextPutAll: String cr ].
	fileStream nextPutAll: String cr
]

{ #category : #writing }
ClassDocBuilder class >> writeComment [
"writes in the file the name of the class"
	fileStream nextPutAll: String cr; nextPutAll: '## Comment'; nextPutAll: String cr; nextPutAll: String cr;
				  nextPutAll: '> ';
				  nextPutAll: (class comment);
				  nextPutAll: String cr
]

{ #category : #writing }
ClassDocBuilder class >> writeDoc: aClass [
	class := aClass.
	(self doesFileAlreadyExists)
		ifTrue: [ ^ self ].
	fileStream := self createFileStream.
	self
		writeName;
		writeSuperclass;
		writeComment;
		writePackage;
		writeSubclasses;
		writeInstanceVariables;
		writeClassVariables;
		writeSelectors.
		fileStream close
]

{ #category : #writing }
ClassDocBuilder class >> writeInstanceVariables [
	"writes in the file the name of the instance variables"

	fileStream
		nextPutAll: String cr;
		nextPutAll: '## Instance Variables';
		nextPutAll: String cr;
		nextPutAll: String cr.
	class instVarNames
		do: [ :each | 
			fileStream
				nextPutAll: '- ';
				nextPutAll: each;
				nextPutAll: String cr ].
	fileStream nextPutAll: String cr
]

{ #category : #writing }
ClassDocBuilder class >> writeName [
"writes in the file the name of the class"
	fileStream nextPutAll: String cr; nextPutAll: '# Name'; nextPutAll: String cr; nextPutAll: String cr;
				  nextPutAll: (class name);
				  nextPutAll: String cr
]

{ #category : #writing }
ClassDocBuilder class >> writePackage [
"writes in the file the package of the class"
	fileStream nextPutAll: String cr; nextPutAll: '## Package'; nextPutAll: String cr; nextPutAll: String cr;
				  nextPutAll: (class package name);
				  nextPutAll: String cr
]

{ #category : #writing }
ClassDocBuilder class >> writeSelectors [
"writes in the file the name of the selectors"
	fileStream nextPutAll: String cr; nextPutAll: '## Selectors '; nextPutAll: String cr; nextPutAll: String cr.
	class selectors do: [ :each | fileStream nextPutAll: '- ';
																	nextPutAll: each;
																	nextPutAll: String cr ].
	fileStream nextPutAll: String cr
]

{ #category : #writing }
ClassDocBuilder class >> writeSubclasses [
"writes in the file the name of the subclasses if they exist, write that none exists otherwise"
	fileStream nextPutAll: String cr; nextPutAll: '## Subclasses '; nextPutAll: String cr; nextPutAll: String cr.
	class hasSubclasses ifTrue: [ class subclasses do: [ :each | fileStream nextPutAll: '- ';
																				  nextPutAll: each name;
																				  nextPutAll: String cr ] ]
							 ifFalse: [ fileStream nextPutAll: 'This class has no subclasses.' ].
	fileStream nextPutAll: String cr
]

{ #category : #writing }
ClassDocBuilder class >> writeSuperclass [ 
"writes in the file the name of the superclass"
	fileStream nextPutAll: String cr; nextPutAll: '## Superclass'; nextPutAll: String cr; nextPutAll: String cr;
				  nextPutAll: (class superclass name); nextPutAll: String cr
]
