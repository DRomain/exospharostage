# ExosPharoStage

Exercices demandés pour le stage chez RMod

## Exercice 1

> Coder une liste chaînée en Pharo avec des tests (voir mieux en test driven development).

Contenu de l'exercice dans les packages Chaine et ChaineTest.

## Exercice 2

> Coder une « javadoc » qui prend une classe ou un package en paramètre et qui génère pour chaque classe un fichier avec le nom de la classe, sa super classe, ses sous classes, ses variables d’instance, ses méthodes.

Contenu de l'exercice dans le package DocBuilder.
